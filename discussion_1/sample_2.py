#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import argparse

def my_other_function(a):
	a = 90000
	print("Value inside function: " + str(a))


def my_function(input_list=[1, 2, 3]):
	a = 99
	lol = input_list
	for i in range(0, len(lol)):
		lol[i] = lol[i] ** 2
		print(input_list[i])

	# for i in range(0, len(input_list)):
	# 	input_list[i] = input_list[i] ** 2
	# 	print(input_list[i])


def main():
	for param in sys.argv:
		print(param)

	parser = argparse.ArgumentParser(description='What my program does')
	parser.add_argument('-v', '--verbose', action='store_true', help='Run with extra messages')
	parser.add_argument('-d', '--some_number', type=int, choices=[0, 1, 2], default=1, help='Some number')
 
	args = parser.parse_args()
	print(args.verbose)
	print(args.some_number)

	print("--------------------------------------")

	my_function()
	print("-----------")
	my_function()
	
	print("--------------------------------------")
	b = 888
	my_other_function(b)
	my_other_function(77777)
	print("Value outside function: " + str(b))


if __name__ == "__main__":
	main()
